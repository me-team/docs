# Football docs

## Field dimension
Ratio: 100x65

## Protocol

### Init 
The first message sent by the server to the client.

### Request to play
Client ask server to play. It's like READY UP.

### Waiting for player 2
Message after **init** if you are the first player how logged in. (Player 2 doesn't get this message)

### Client update
Updates the client with the new state. Sent with every tick.
<details>
<summary>Sample</summary>
``` json
{
    "guid": "ASDVSDVDSBSFG"
    "team1":{
        "players":[
            {
                "id":1,
                "position":{
                    "x":1.0000,
                    "y":1.00000
                }
            }
        ]
    },
    "team2":{
        "players":[
            {
                "id":1,
                "position":{
                    "x":1.0000,
                    "y":1.00000
                }
            }
        ]
    }
    "ball"{
        "position":{
            "x":1.00000,
            "y":1.00000
        },
        "direction":{
            "x":1.0000,
            "y":1.00000
        }
        "speed": 0.0000
    }
}
```
</details>
